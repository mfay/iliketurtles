﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Encoder
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 2)
            {
                Console.WriteLine("You must specify the file to encode and the output file.");
                return;
            }

            var inputFile = args[0];
            var outputFile = args[1];
            if (!File.Exists(inputFile) || string.IsNullOrEmpty(outputFile))
            {
                throw new ArgumentException("Please specifiy the file to encode as well as an output file.");
            }

            var data = File.ReadAllBytes(inputFile);
            string encodedData = Convert.ToBase64String(data);
            using (var writer = new StreamWriter(outputFile))
            {
                writer.WriteLine(encodedData);
            }
        }
    }
}
