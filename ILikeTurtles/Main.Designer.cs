﻿namespace ILikeTurtles
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pbTurtle = new System.Windows.Forms.PictureBox();
            this.timerHide = new System.Windows.Forms.Timer(this.components);
            this.timerShow = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pbTurtle)).BeginInit();
            this.SuspendLayout();
            // 
            // pbTurtle
            // 
            this.pbTurtle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbTurtle.Image = global::ILikeTurtles.Properties.Resources.turtledance;
            this.pbTurtle.Location = new System.Drawing.Point(0, 0);
            this.pbTurtle.Name = "pbTurtle";
            this.pbTurtle.Size = new System.Drawing.Size(253, 292);
            this.pbTurtle.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbTurtle.TabIndex = 0;
            this.pbTurtle.TabStop = false;
            // 
            // timerHide
            // 
            this.timerHide.Interval = 5000;
            this.timerHide.Tick += new System.EventHandler(this.timerHide_Tick);
            // 
            // timerShow
            // 
            this.timerShow.Tick += new System.EventHandler(this.timerShow_Tick);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Fuchsia;
            this.ClientSize = new System.Drawing.Size(253, 292);
            this.Controls.Add(this.pbTurtle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Main";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "I Like Turtles";
            this.TopMost = true;
            this.TransparencyKey = System.Drawing.Color.Fuchsia;
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            this.Load += new System.EventHandler(this.Main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbTurtle)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbTurtle;
        private System.Windows.Forms.Timer timerHide;
        private System.Windows.Forms.Timer timerShow;
    }
}

