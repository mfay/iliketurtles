﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ILikeTurtles
{
    public partial class Main : Form
    {
        Stream stream;
        System.Media.SoundPlayer player;

        public Main()
        {
            InitializeComponent();

            Visible = false;

            //RegisterOnStartUp();

            var names = Assembly.GetExecutingAssembly().GetManifestResourceNames();
            stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("ILikeTurtles.Resources.I-like-turtles.wav");
            player = new System.Media.SoundPlayer(stream);
        }

        //protected override CreateParams CreateParams
        //{
        //    get
        //    {
        //        var Params = base.CreateParams;
        //        Params.ExStyle |= 0x80;
        //        return Params;
        //    }
        //}

        private void timerHide_Tick(object sender, EventArgs e)
        {
            if (Visible)
            {
                Visible = false;
                //WindowState = FormWindowState.Minimized;
                timerHide.Enabled = false;
            }
        }

        private void timerShow_Tick(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Normal;
            Visible = true;
            SetTimerInterval();
            timerHide.Enabled = true;

            Thread.Sleep(2000);

            player.Play();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            SetTimerInterval();
        }

        private void SetTimerInterval()
        {
#if DEBUG
            // set the timer 5 seconds
            var interval = 10000;
#else
            // set the timer to random interval 30 minutes +- 5 minutes
            var interval = 1000 * 60 * (24 + new Random().Next(1, 10));
#endif
            timerShow.Interval = interval;
            timerShow.Enabled = true;
        }

        private void RegisterOnStartUp()
        {
            RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            var val = registryKey.GetValue("ILikeTurtles");
            if (val == null)
            {
                registryKey.SetValue("ILikeTurtles", Application.ExecutablePath);
            }
        }

    }
}
