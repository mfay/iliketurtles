﻿namespace NotePad
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnInfect = new System.Windows.Forms.Button();
            this.btnCure = new System.Windows.Forms.Button();
            this.lblMessage = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnInfect
            // 
            this.btnInfect.Location = new System.Drawing.Point(21, 12);
            this.btnInfect.Name = "btnInfect";
            this.btnInfect.Size = new System.Drawing.Size(103, 48);
            this.btnInfect.TabIndex = 0;
            this.btnInfect.Text = "Infect";
            this.btnInfect.UseVisualStyleBackColor = true;
            this.btnInfect.Click += new System.EventHandler(this.btnInfect_Click);
            // 
            // btnCure
            // 
            this.btnCure.Location = new System.Drawing.Point(130, 12);
            this.btnCure.Name = "btnCure";
            this.btnCure.Size = new System.Drawing.Size(103, 48);
            this.btnCure.TabIndex = 1;
            this.btnCure.Text = "Cure";
            this.btnCure.UseVisualStyleBackColor = true;
            this.btnCure.Click += new System.EventHandler(this.btnCure_Click);
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblMessage.Location = new System.Drawing.Point(0, 352);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(0, 20);
            this.lblMessage.TabIndex = 2;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(801, 372);
            this.Controls.Add(this.lblMessage);
            this.Controls.Add(this.btnCure);
            this.Controls.Add(this.btnInfect);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Main";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "NotePad";
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            this.Load += new System.EventHandler(this.Main_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnInfect;
        private System.Windows.Forms.Button btnCure;
        private System.Windows.Forms.Label lblMessage;
    }
}